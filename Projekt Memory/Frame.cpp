﻿#include "Frame.h"

/**
 * \brief Stwóż obsługę kalrek w konsoli
 * \param console Konsola gdzie mam rysować.
 */
Frame::Frame(Console &console)
{
	this->console = &console;
	FrameSize = this->console->GetConsoleSize();
	newFrame = new CharInfoString[FrameSize.Y];
	oldFrame = new CharInfoString[FrameSize.Y];

	for (int i = 0; i < FrameSize.Y; ++i)
	{
		newFrame[i] = FrameSize.X;
		oldFrame[i] = FrameSize.X;
	}
}

/**
 * \brief Narysuj w konsoli aktualną klatke.
 */
void Frame::drawFrame() const
{
	for (int i = 0; i < FrameSize.Y; ++i)
	{
		if (!(newFrame[i]==oldFrame[i]))
		{
			console->writeLine(newFrame[i], i);
		}
		oldFrame[i].setCharInfos(0, newFrame[i]);
	}
}

/**
 * \brief bardzo niewydajne. TYLKO W przypadku artefaktów.
 */
void Frame::clearFrame() const
{
	console->setCursorPosition(*new COORD{ 0,0 });

	for (int i = 0; i < FrameSize.Y; ++i)
	{
		newFrame[i] = FrameSize.X;
		oldFrame[i] = FrameSize.X;
		for (int j = 0; j < FrameSize.X; ++j)
		{
			cout << ' ';
		}
	}
}

/**
 * \brief dodaj na wierzcu kaltki jeden wiersz.
 * \param wiersz który wiersz mam dodać. 
 * \param str string do stawienia. Dokładnie takiej samej długości jak szerokość klatki
 * \return 0: sukces -1 błąd.
 */
int Frame::addToFrame(const int wiersz, CharInfoString str) const
{
	//sprawdź czy jest odpowiedniej wielkości
	if (FrameSize.X==str.getSize())
	{
		newFrame[wiersz] = str;
		return 0;
	}
	return -1;
}


/**
 * \brief Dodaj na wierzhu klatki string krótszy niż szerokość klatki.
 * \param punkt Z kąd zaczynam dodawać?
 * \param str co mam dodać?
 * \return 0: sukces -1 błąd.
 */
int Frame::addToFrame(const COORD punkt, CharInfoString str) const
{
	if (punkt.X+str.getSize()<FrameSize.X && punkt.Y<FrameSize.Y)
	{
		newFrame[punkt.Y].setCharInfos(punkt.X, str);
		return 0;
	}
	return -1;
}

/**
 * \brief Wstaw Kwadrat(tablice) CkarInfoStringów na wierzchu klatki
 * \param punkt Gdzie mam zacząc? Lewy Górny róg.
 * \param str tablica do wstawienia. wielkości Rozmiar.y
 * \param rozmiar coord zawireający rozmiary do wstawienia
 * rozmiar.y== wielkość tabeli
 * rozmiar.x==długość stringów w tablei.
 * \return 
 */
int Frame::addToFrame(const COORD punkt, CharInfoString* str, const COORD rozmiar) const
{
	//sprawdź czy się zmieści
	if (!willItFit(punkt,rozmiar))
	{
		return -1;
	}
	
	for (int i = 0; i < rozmiar.Y; ++i)
	{
		newFrame[punkt.Y + i].setCharInfos(punkt.X, str[i]);
	}
	return 0;
}



/**
 * \brief Wyszyść aktualną klatke i przygotuj się do nowej klatki.
 * \return zawsze 0;
 */
int Frame::newframe() const
{
	for (int i = 0; i < FrameSize.Y; ++i)
	{
		newFrame[i] = FrameSize.X;
	}
	return 0;
}

/**
 * \brief Wstaw porstokąt do klatki
 * \param punkt lewy Górny róg. gdzie zacząć.
 * \param Rozmiar rozmiar prostokąta
 * \param wypelnienie jakim znakiem wypełnić ten prostokąt.
 * \param kolor Attributesy do wstawienia.
 * \return 0: sukces -1 błąd.
 */
int Frame::addRectToFrame(const COORD punkt, const COORD Rozmiar, const char wypelnienie, const WORD kolor) const
{
	if(willItFit(punkt, Rozmiar))
	{
		for (int i = 0; i < Rozmiar.Y; ++i)
		{
			newFrame[punkt.Y + i].setChars(punkt.X, wypelnienie, Rozmiar.X);
			newFrame[punkt.Y + i].setAttributes(kolor, punkt.X, Rozmiar.X);
		}
		return 0;
	}
	return -1;
}

/**
 * \brief to samo co addRectToFrame(coord,coord,char) tylko pierwsze dwa argumeny są intami.
 * \return 0: sukces -1 błąd.
 */
int Frame::addRectToFrame(const int x, const int y, const int width, const int height, const char wypelnienie) const
{
	if (willItFit(*new COORD{ static_cast<short>(x),static_cast<short>(y) }, *new COORD{ static_cast<short>(width),static_cast<short>(height)}))
	{
		for (int i = 0; i < height; ++i)
		{
			newFrame[y + i].setChars(x, wypelnienie, width);
		}
		return 0;
	}
	return -1;
}

/**
 * \brief Sprawdza czy prostokąt o rozmiaże Rozmiar wstawiony w Punkt nie wyjedzie poza klatke.
 * \param punkt gdzie wstawiam? lewy górny róg.
 * \param Rozmiar rozmiar prostokąta.
 * \return false jeżeli się nie miści w klatce; true jeżeli się zmieści.
 */
bool Frame::willItFit(const COORD punkt, const COORD Rozmiar) const
{
	//dodatni punkt
	if (punkt.X < 0 || punkt.Y < 0)
	{
		return false;
	}

	//sprawdź czy się zmieści
	if (!(punkt.X + Rozmiar.X < FrameSize.X && punkt.Y + Rozmiar.Y < FrameSize.Y))
	{
		return false;
	}
	return true;
}

