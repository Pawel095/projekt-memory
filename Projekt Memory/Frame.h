﻿#pragma once
#include "Console.h"

class Frame
{
public:
	~Frame() = default;
	Frame(Console &console);
	void drawFrame() const;
	void clearFrame() const;
	int addToFrame(int wiersz,CharInfoString str) const;
	int addToFrame(COORD punkt,CharInfoString str) const;
	int addToFrame(COORD punkt,CharInfoString* str,COORD rozmiar) const;
	int newframe() const;

	int addRectToFrame(COORD punkt, COORD Rozmiar,char wypelnienie=static_cast<char>(219),WORD kolor= FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED) const;
	int addRectToFrame(int x,int y, int width,int height,char wypelnienie=static_cast<char>(219)) const;

	bool willItFit(COORD punkt, COORD Rozmiar) const;

private:
	COORD FrameSize;
	CharInfoString* newFrame;
	CharInfoString* oldFrame;
	Console *console;
};
