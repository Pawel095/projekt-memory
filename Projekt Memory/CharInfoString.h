#pragma once
#include <Windows.h>
#include <string>

class CharInfoString
{
public:
	CharInfoString();
	~CharInfoString() = default;

	CharInfoString(char* x);
	CharInfoString(int x);

	void operator=(char* x);
	void operator=(char x);
	void operator=(int x);

	void operator<<(const char * x);
	void operator<<(const char x);
	void operator<<(const CharInfoString x);

	CHAR_INFO operator[](int x) const;
	bool operator==(CharInfoString a) const;
	WORD getAttribute(int index) const;
	void setAttributes(WORD Attribute, int start=0, int howMuch=-1) const;
	unsigned int getSize() const;
	int setChars(int start,char* text);
	int setChars(int start,char text) const;
	int setChars(int start,char text,int ile,WORD attribures= FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	int setCharInfos(int start,CharInfoString text);
private:
	unsigned int size;
	CHAR_INFO* text;
	int cursor = 0;
};