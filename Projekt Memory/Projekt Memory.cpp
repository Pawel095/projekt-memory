#include "CharInfoString.h"
#include "Console.h"
#include "Frame.h"
#include "Karta.h"
#include <ctime>

//makra tutaj!
#define PLANSZA_8_8 0b0001
#define PLANSZA_12_12 0b0010
#define WALKA_KONTRA_AI 0b0100
#define WALKA_KONTRA_CZLOWIEK 0b1000

struct PlayerInfo
{
	int punkty = 0;
	int seria = 0;
	int najlepszaSeria = 0;
};

int keyEventProcessor(const KEY_EVENT_RECORD ker, Console c)
{
	if (ker.bKeyDown)
	{
		return ker.wVirtualKeyCode;
	}
	return 0;
}

int mouseEventProcessor(const MOUSE_EVENT_RECORD mer, Console c, COORD &mouseCoords)
{
	mouseCoords.X = mer.dwMousePosition.X;
	mouseCoords.Y = mer.dwMousePosition.Y;
	return mer.dwButtonState;
}

uint8_t menu(Console &console,Frame &frame)
{
	const COORD windowSize = *new COORD{ (console.GetConsoleSize().X - 1) / 2,(console.GetConsoleSize().Y - 1) / 2 };
	const COORD punkt = *new COORD{ (console.GetConsoleSize().X - 1) / 4,(console.GetConsoleSize().Y - 1) / 4 };
	
	CharInfoString tekstWMenu[3] = { *new CharInfoString{"Wybierz rozmiar Planszy: "},*new CharInfoString{"8 na 8"},*new CharInfoString{ "12 na 12" } };
	COORD pozycjeWMenu[3] = { *new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(tekstWMenu[0].getSize() / 2),punkt.Y + windowSize.Y * 1 / 4},
							  *new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(tekstWMenu[1].getSize() / 2),punkt.Y + windowSize.Y * 2 / 4 },
							  *new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(tekstWMenu[2].getSize() / 2),punkt.Y + windowSize.Y * 3 / 4 } };
	uint8_t opcja1 = 0;

	bool koniec = false;
	while (!koniec)
	{
		//eventy
		const EventReturner eRet = console.ProcessEvents(keyEventProcessor, mouseEventProcessor);
		if (eRet.which == console.KEYBOARD_EVENT)
		{
			switch (eRet.returnValue)
			{
			case VK_ESCAPE:
				ExitProcess(0);
			case VK_UP:
				tekstWMenu[1].setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED, 0, tekstWMenu[1].getSize() - 1);
				tekstWMenu[2].setAttributes(FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN, 0, tekstWMenu[2].getSize() - 1);
				opcja1 = PLANSZA_8_8;
				break;
			case VK_DOWN:
				tekstWMenu[2].setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED, 0, tekstWMenu[2].getSize() - 1);
				tekstWMenu[1].setAttributes(FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN, 0, tekstWMenu[1].getSize() - 1);
				opcja1 = PLANSZA_12_12;
				break;
			case VK_RETURN:
				if (opcja1 !=0)
				{
					koniec = true;
				}
			default: 
				break;
			}
		}

		//ramka
		frame.addRectToFrame(punkt, windowSize);
		frame.addRectToFrame(punkt.X + 1, punkt.Y + 1, windowSize.X - 2, windowSize.Y - 2, ' ');

		// u góry
		CharInfoString temp = "Menu";
		temp.setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		frame.addToFrame(*new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(temp.getSize() / 2),punkt.Y }, temp);

		frame.addToFrame(pozycjeWMenu[0], tekstWMenu[0]);
		frame.addToFrame(pozycjeWMenu[1], tekstWMenu[1]);
		frame.addToFrame(pozycjeWMenu[2], tekstWMenu[2]);

		frame.drawFrame();
	}
	frame.clearFrame();

	tekstWMenu[0] = *new CharInfoString{ "Kim Jest Gracz 2?" };
	tekstWMenu[1] = *new CharInfoString{ "Komputer" };
	tekstWMenu[2] = *new CharInfoString{ "Czlowiek" };

	pozycjeWMenu[0] = *new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(tekstWMenu[0].getSize() / 2),punkt.Y + windowSize.Y * 1 / 4 };
	pozycjeWMenu[1] = *new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(tekstWMenu[1].getSize() / 2),punkt.Y + windowSize.Y * 2 / 4 };
	pozycjeWMenu[2] = *new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(tekstWMenu[2].getSize() / 2),punkt.Y + windowSize.Y * 3 / 4 };

	uint8_t opcja2 = 0;

	while (true)
	{
		//eventy
		const EventReturner eRet = console.ProcessEvents(keyEventProcessor, mouseEventProcessor);
		if (eRet.which == console.KEYBOARD_EVENT)
		{
			switch (eRet.returnValue)
			{
			case VK_ESCAPE:
				ExitProcess(0);
			case VK_UP:
				tekstWMenu[1].setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED, 0, tekstWMenu[1].getSize() - 1);
				tekstWMenu[2].setAttributes(FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN, 0, tekstWMenu[2].getSize() - 1);
				opcja2 = WALKA_KONTRA_AI;
				break;
			case VK_DOWN:
				tekstWMenu[2].setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED, 0, tekstWMenu[2].getSize() - 1);
				tekstWMenu[1].setAttributes(FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN, 0, tekstWMenu[1].getSize() - 1);
				opcja2 = WALKA_KONTRA_CZLOWIEK;
				break;
			case VK_RETURN:
				if (opcja2!=0)
				{
					return opcja1 | opcja2;
				}
				break;
			default:
				break;
			}
		}

		//ramka
		frame.addRectToFrame(punkt, windowSize);
		frame.addRectToFrame(punkt.X + 1, punkt.Y + 1, windowSize.X - 2, windowSize.Y - 2, ' ');

		// u góry
		CharInfoString temp = "Menu";
		temp.setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		frame.addToFrame(*new COORD{ punkt.X + windowSize.X / 2 - static_cast<short>(temp.getSize() / 2),punkt.Y }, temp);

		frame.addToFrame(pozycjeWMenu[0], tekstWMenu[0]);
		frame.addToFrame(pozycjeWMenu[1], tekstWMenu[1]);
		frame.addToFrame(pozycjeWMenu[2], tekstWMenu[2]);

		frame.drawFrame();
	}

}

void gra(Console &console,Frame &frame, const int co)
{
	//setup:
	//srand(time(nullptr));

	srand(0);

	bool walczeZKomputerem=false;
	int x=8, y=8;
	if (co & PLANSZA_8_8)
	{
		x = 8; 
		y = 8;
	}
	if (co & PLANSZA_12_12)
	{
		x = 12;
		y = 12;
	}
	if (co & WALKA_KONTRA_AI)
	{
		walczeZKomputerem = true;
	}
	if (co & WALKA_KONTRA_CZLOWIEK)
	{
		walczeZKomputerem = false;
	}


	const int ILE_TYPOW_KART = x*y / 2;
	const COORD SIZE_IN_CHARS{static_cast<short>(2 * x + 2),static_cast<short>(2 * y)};
	const COORD PLANSZA_START_POINT{ static_cast<short>(console.GetConsoleSize().X / 2 - SIZE_IN_CHARS.X / 2),
							 static_cast<short>(console.GetConsoleSize().Y / 2 - SIZE_IN_CHARS.Y / 2) };

	//separator = "├─┼─┼─┼─┼─┼─┼─┼─┤";
	//     gora = "┌─┬─┬─┬─┬─┬─┬─┬─┐"
	//      dol = "└─┴─┴─┴─┴─┴─┴─┴─┘"
	CharInfoString separator = SIZE_IN_CHARS.X;
	CharInfoString gora = SIZE_IN_CHARS.X;
	CharInfoString dol = SIZE_IN_CHARS.X;
	for (int i = 0; i < SIZE_IN_CHARS.X-1; ++i)
	{
		if (i % 2 == 0)
		{
			//┼
			separator.setChars(i, static_cast<char>(197));
			//┬
			gora.setChars(i, static_cast<char>(194));
			//┴
			dol.setChars(i, static_cast<char>(193));
		}
		if (i == 0) 
		{
			//├
			separator.setChars(0, static_cast<char>(195));
			//┌
			gora.setChars(i, static_cast<char>(218));
			//└
			dol.setChars(i, static_cast<char>(192));
		}
		if (i % 2 == 1)
		{
			//─
			separator.setChars(i, static_cast<char>(196));
			gora.setChars(i, static_cast<char>(196));
			dol.setChars(i, static_cast<char>(196));
		}
		if (i == SIZE_IN_CHARS.X - 2)
		{
			//┤
			separator.setChars(i, static_cast<char>(180));
			//┐
			gora.setChars(i, static_cast<char>(191));
			//┘
			dol.setChars(i, static_cast<char>(217));
		}
	}
	frame.clearFrame();

	//czy się plansza zmieści?
	if (!frame.willItFit(*new COORD{ 0,1 }, SIZE_IN_CHARS))
	{
		cout << "Za male okno konsoli. rozmiar co najmniej" << SIZE_IN_CHARS.X << " na " << SIZE_IN_CHARS.Y + 1 << endl;
		system("pause");
		ExitProcess(-1);
	}

	//zajmowanie pamięci
	Karta **plansza = new Karta*[x];
	COORD **kordyKart = new COORD*[x];
	for (int i = 0; i < y; ++i)
	{
		plansza[i] = new Karta[y];
		kordyKart[i] = new COORD[y];
	}

	//wstawianie koordów do tablicy.
	for (int i = 0; i < x; ++i)
	{
		for (int j = 0; j < y; ++j)
		{
			kordyKart[i][j] = *new COORD{static_cast<short>(i*2+1+PLANSZA_START_POINT.X),static_cast<short>(j*2+1+PLANSZA_START_POINT.Y)};
		}
	}

	//generowanie losowych kart.

	//lista znaków na kartach
	char *typyKart = new char[ILE_TYPOW_KART];
	int *ileUzyte = new int[ILE_TYPOW_KART];

	for (int i = 0; i <ILE_TYPOW_KART; ++i)
	{
		typyKart[i] = static_cast<char>('!'+i);
		ileUzyte[i] = 0;
	}


	//stwóz plansz gdzie są 2 karty każdego rodzaju.

	for (int i = 0; i < x; ++i)
	{
		for (int j = 0; j < y; ++j)
		{
			bool wstawione = false;
			while (!wstawione)
			{
				const int tempRand = rand() % ILE_TYPOW_KART;
				if (ileUzyte[tempRand]<2)
				{
					ileUzyte[tempRand]++;
					plansza[i][j].znak = typyKart[tempRand];
					plansza[i][j].odkryta = false;
					wstawione = true;
				}
			}
		}
	}

	//render

	COORD odkryte[2] = 
	{ 
		{-1,-1},
		{-1,-1} 
	};
	PlayerInfo playerInfo[2];
	int pozostaloPar = ILE_TYPOW_KART;
	
	int aktualnyGraczNr = 0;
	int ileAktualnieOdkryte = 0;

	//bonusowe sterowanie
	bool pokazanieStartowe = true;
	bool enableUserInput = false;
	const clock_t pokazanieStartoweClock = clock();

	//sterowanie od ai
	clock_t aiClock=clock();
	bool wybieram2 = false;

	while (true)
	{
		//eventy
		
		frame.newframe();

		if (enableUserInput)
		{
			const EventReturner eRet = console.ProcessEvents(keyEventProcessor, mouseEventProcessor);
			if (eRet.which == console.KEYBOARD_EVENT)
			{
				switch (eRet.returnValue)
				{
				case VK_ESCAPE:
					ExitProcess(0);
				default:
					break;
				}
			}

			if (eRet.which==console.MOUSE_EVENTC)
			{
				bool found = false;
				switch (eRet.returnValue)
				{
				case FROM_LEFT_1ST_BUTTON_PRESSED:

					//znajdz karte która została kliknieta.
					for (int i = 0; i < x && !found; ++i)
					{
						for (int j = 0; j < y && !found; ++j)
						{
							if (eRet.mouseCoord.X == kordyKart[i][j].X && eRet.mouseCoord.Y == kordyKart[i][j].Y)
							{
								if (!plansza[i][j].odkryta)
								{
									plansza[i][j].odkryta = true;
									found = true;
									odkryte[ileAktualnieOdkryte].X = i;
									odkryte[ileAktualnieOdkryte].Y = j;
									ileAktualnieOdkryte++;
								}
							}
						}
					}
					break;
				default: 
					break;
				}
			}
		}
		//TU NARAZIE JEST ŚCIERNISKO, ALE BĘDZIE AI ŚRODOWISKO!

		if (walczeZKomputerem && aktualnyGraczNr == 1)
		{
			while (true)
			{
				int a = rand() % x, b = rand() % y, c = rand() % x, d = rand() % y;
				if (!plansza[a][b].odkryta && !plansza[c][d].odkryta)
				{
					plansza[a][b].odkryta = true;
					plansza[c][d].odkryta = true;
					odkryte[0].X = a;
					odkryte[0].Y = b;
					odkryte[1].X = c;
					odkryte[1].Y = d;
					ileAktualnieOdkryte = 2;
					break;
				}
			}
		}




		//render planszy
		frame.addToFrame(PLANSZA_START_POINT, gora);
		frame.addToFrame(*new COORD{ PLANSZA_START_POINT.X,PLANSZA_START_POINT.Y + SIZE_IN_CHARS.Y },dol);

		for (int i = 0; i < SIZE_IN_CHARS.Y; i+=2)
		{
			CharInfoString wiersz = SIZE_IN_CHARS.X;
			wiersz << static_cast<char>(179);

			for (int j = 0; j < x; ++j)
			{

				if (plansza[j][i/2].odkryta || pokazanieStartowe)
				{
					wiersz << plansza[j][i/2].znak;
				}
				else
				{
					wiersz << static_cast<char>(219);
				}
				wiersz << static_cast<char>(179);
			}
			frame.addToFrame(*new COORD{ PLANSZA_START_POINT.X,static_cast<char>(i + 1+PLANSZA_START_POINT.Y) }, wiersz);

			if (i<SIZE_IN_CHARS.Y-2)
			{
				frame.addToFrame(*new COORD{ PLANSZA_START_POINT.X,static_cast<char>(i + 2+PLANSZA_START_POINT.Y) }, separator);
			}
		}

		//render paska info.
		const int PER_ELEMENT = console.GetConsoleSize().X/5;

		CharInfoString infobar = console.GetConsoleSize().X;
		infobar.setAttributes(BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);
		
		infobar.setChars(PER_ELEMENT * 0, "Punkty Gracza 1:");
		infobar << to_string(playerInfo[0].punkty).c_str();

		infobar.setChars(PER_ELEMENT * 1, "Punkty Gracza 2:");
		infobar << to_string(playerInfo[1].punkty).c_str();

		infobar.setChars(PER_ELEMENT * 2, "Max seria Gracza 1:");
		infobar << to_string(playerInfo[0].najlepszaSeria).c_str();

		infobar.setChars(PER_ELEMENT * 3, "Max seria Gracza 2:");
		infobar << to_string(playerInfo[1].najlepszaSeria).c_str();

		infobar.setChars(PER_ELEMENT * 4, "Pozostale pary:");
		infobar << to_string(pozostaloPar).c_str();
		frame.addToFrame(0, infobar);


		CharInfoString ktoTerazGra = console.GetConsoleSize().X; //18;
		ktoTerazGra.setChars(console.GetConsoleSize().X/2 - 18/2, "RUNDA GRACZA NR ");
		ktoTerazGra << to_string(aktualnyGraczNr+1).c_str();
		frame.addToFrame(PLANSZA_START_POINT.Y-1,ktoTerazGra);

		const double odliczanieClock = double(clock() - pokazanieStartoweClock) / CLOCKS_PER_SEC;
		//odliczanie na środku
		if (pokazanieStartowe && odliczanieClock>2.0f)
		{
			pokazanieStartowe = false;
			enableUserInput = true;
		}

		frame.drawFrame();

		if (ileAktualnieOdkryte>2)
		{
			ImDead("Na wiecej niz 2 karty nie dam rady grac...");
		}


		//HERE THERE BE GAME LOGIC!
		if (ileAktualnieOdkryte>1)
		{
			if (plansza[odkryte[0].X][odkryte[0].Y].znak == plansza[odkryte[1].X][odkryte[1].Y].znak)
			{
				ileAktualnieOdkryte = 0;
				playerInfo[aktualnyGraczNr].punkty++;
				playerInfo[aktualnyGraczNr].seria++;
				pozostaloPar--;
			}
			else
			{
				ileAktualnieOdkryte = 0;
				plansza[odkryte[0].X][odkryte[0].Y].odkryta = false;
				plansza[odkryte[1].X][odkryte[1].Y].odkryta = false;
				if (playerInfo[aktualnyGraczNr].najlepszaSeria < playerInfo[aktualnyGraczNr].seria)
				{
					playerInfo[aktualnyGraczNr].najlepszaSeria = playerInfo[aktualnyGraczNr].seria;
				}
				playerInfo[aktualnyGraczNr].seria = 0;
				
				Sleep(1000);
				if (aktualnyGraczNr == 0)
				{
					aktualnyGraczNr = 1;
				}
				else
				{
					aktualnyGraczNr = 0;
				}
			}
		}
	}
}

int main()
{
	Console console = Console();
	console.SetMode(ENABLE_MOUSE_INPUT);

	Frame frame = Frame(console);
	const int co = menu(console, frame);
	gra(console, frame, co);
	system("pause");
    return 0;
}
