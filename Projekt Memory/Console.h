﻿#pragma once
#include <windows.h>
#include <iostream>
#include "CharInfoString.h"
using namespace std;

void ImDead(char* Message);

struct EventReturner
{
	int which;
	int returnValue;
	COORD mouseCoord;
};

class Console
{
public:
	const int KEYBOARD_EVENT = 5;
	const int MOUSE_EVENTC = 6;

	void setCursorPosition(COORD punkt) const;
	void writepixel(COORD punkt,CharInfoString znak) const;
	void writeLine(CharInfoString line, int wiersz = -1) const;
	EventReturner ProcessEvents(int(*keyEvent)(KEY_EVENT_RECORD,Console), int(*mouseEvent)(MOUSE_EVENT_RECORD,Console,COORD&)) const;

	void SetMode(DWORD mode) const;
	
	void setCursorInfo(CONSOLE_CURSOR_INFO cinfo);
	CONSOLE_CURSOR_INFO getCursorInfo() const;

	COORD GetConsoleSize() const;
	HANDLE GetInHandle() const;
	HANDLE GetOutHandle() const;

	Console::Console();
	~Console() = default;

private:
	COORD cursorLocation;
	COORD consoleSize;
	HANDLE outHandle;
	HANDLE inHandle;
	CONSOLE_CURSOR_INFO cursorInfo;
	CONSOLE_SCREEN_BUFFER_INFO BufferInfo;
};