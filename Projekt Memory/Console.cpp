﻿#include "Console.h"


/**
 * \brief Pokaż błąd w winapi i zakończ proces.
 * \param message Wiadomość do wyświetlenia.
 */
void ImDead(char* message)
{
	std::cerr << message << endl;
	cerr << "Error code: " << hex << GetLastError() << endl;
	system("pause");
	ExitProcess(0);
}

Console::Console()
{
	//inicjalizacja wszystkich zmiennych
	cursorLocation.X = 0;
	cursorLocation.Y = 0;

	outHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	inHandle = GetStdHandle(STD_INPUT_HANDLE);

	GetConsoleCursorInfo(outHandle, &cursorInfo);
	GetConsoleScreenBufferInfo(outHandle, &BufferInfo);

	consoleSize.X = BufferInfo.srWindow.Right - BufferInfo.srWindow.Left + 1;
	consoleSize.Y = BufferInfo.srWindow.Bottom - BufferInfo.srWindow.Top;

	//Ukryj kursor
	cursorInfo.bVisible = FALSE;
	SetConsoleCursorInfo(outHandle, &cursorInfo);
}

/**
 * \brief Ustaw tryb konsoli. 
 * Wrapper do Funkcji SetCOnsoleMode(handle,mode)
 * \param mode tryb do Ustawienia.
 */
void Console::SetMode(const DWORD mode) const
{
	SetConsoleMode(inHandle, mode);
}

/**
 * \brief Zwraca Handle dla wejścia w konsoli.
 * \return Zwraca Handle dla wejścia w konsoli. 
 */
HANDLE Console::GetInHandle() const
{
	return inHandle;
}

/**
 * \brief  Zwraca Handle dla wyjścia w konsoli.
 * \return  Zwraca Handle dla wyjścia w konsoli.
 */
HANDLE Console::GetOutHandle() const
{
	return outHandle;
}

void Console::setCursorPosition(const COORD punkt) const
{
	SetConsoleCursorPosition(outHandle, punkt);
}

/**
* \brief Bardzo niewydajne wstawianie pixela.
* \param punkt punkt gdzie wstawić znak
* \param znak znak do wstawienia
*/
void Console::writepixel(COORD punkt,CharInfoString znak) const
{

	SetConsoleCursorPosition(outHandle, punkt);
	SetConsoleTextAttribute(outHandle,znak.getAttribute(0));
	cout << znak[0].Char.AsciiChar;
}

/**
 * \brief Wypisz linie do konsoli
 * \param line tablica ze znakami i ich atrybutami.
 * \param wiersz 
 */
void Console::writeLine(CharInfoString line, const int wiersz) const
{
	COORD c;
	if (wiersz==-1)
	{
		c.Y = consoleSize.Y;
		c.X = 0;
	}
	else
	{
		c.Y = wiersz;
		c.X = 0;
	}
	SetConsoleCursorPosition(outHandle, c);
	SetConsoleTextAttribute(outHandle, line[0].Attributes);
	WORD lastAttrs = line[0].Attributes;
	for (unsigned int i = 0; i<line.getSize(); ++i)
	{
		if (line[i].Attributes!=lastAttrs)
		{
			SetConsoleTextAttribute(outHandle, line.getAttribute(i));
			lastAttrs = line[i].Attributes;
		}
		cout << line[i].Char.AsciiChar;
	}
}
/**
 * \brief Finkcja która uruchamia funkcje w argumentach aby obsłużyć eventy.
 * \param keyEvent Funkcja do obsługi zdażeń klawiszy. 
 * Musi zwracać int i przyjmować KEY_EVENT_RECORD, i klasę console jako arbugmenty
 * \param mouseEvent Funkcja do obsługi zdażeń myszki.
 * Musi zwracać int i przyjmować MOUSE_EVENT_RECORD, i klasę console jako arbugmenty
 * \return Struktura Eventreturner.
 */
EventReturner Console::ProcessEvents(int(*keyEvent)(KEY_EVENT_RECORD,Console), int(*mouseEvent)(MOUSE_EVENT_RECORD,Console,COORD&)) const
{
	INPUT_RECORD events[10];
	unsigned long eventsRead;
	EventReturner ret;
	ret.which = 0;
	ret.returnValue = 0;
	ret.mouseCoord = *new COORD{ 0,0 };

	//To Automatycznie Oczekuje na eventy
	if (!ReadConsoleInput(inHandle, events, 10, &eventsRead))
	{
		ImDead("Error in console::ProcessEvents");
	}

	for (unsigned long i = 0; i < eventsRead; ++i)
	{
		
		switch (events[i].EventType)
		{
		case KEY_EVENT:
			ret.which = KEYBOARD_EVENT;
			ret.returnValue=keyEvent(events[i].Event.KeyEvent, *this);
			return ret;
		case MOUSE_EVENT:
			ret.which = MOUSE_EVENTC;
			ret.returnValue=mouseEvent(events[i].Event.MouseEvent,*this,ret.mouseCoord);
			return ret;

		//Te tutaj Ignorujemy.
		case WINDOW_BUFFER_SIZE_EVENT:
		case FOCUS_EVENT:
		case MENU_EVENT:
			break;
		default:
			ImDead("A wild Unknown Event appeared!");
			break;
		}
	}
	return ret;
}


/**
 * \brief Wrapper dla funkcji SetConsoleCursorInfo(handle,CONSOLE_CURSOR_INFO)
 */
void Console::setCursorInfo(const CONSOLE_CURSOR_INFO cinfo)
{
	cursorInfo = cinfo;
	SetConsoleCursorInfo(outHandle, &cursorInfo);
}

/**
 * \brief Zwaca aktualne CONSOLE_CURSOR_INFO; 
 */
CONSOLE_CURSOR_INFO Console::getCursorInfo() const
{
	return cursorInfo;
}


/**
 * \brief Zwraca rozmiar okna konsoli
 * \return COORD x=szerokość y=wysokość.
 */
COORD Console::GetConsoleSize() const
{
	return consoleSize;
}

