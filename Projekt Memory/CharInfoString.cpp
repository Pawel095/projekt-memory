#include <Windows.h>
#include "CharInfoString.h"



CharInfoString::CharInfoString()
{
	cursor = 0;
	size = 0;
	text = nullptr;
}

/**
 * \brief Create string with white text. Size will equal param's size.
 * \param x C-string null terminated. 
 */
CharInfoString::CharInfoString(char* x)
{
	cursor = 0;
	int size = 0;
	while (x[size] != 0)
	{
		size++;
	}
	size += 1;
	this->size = size;

	text = new CHAR_INFO[size];

	for (int i = 0; i < size; ++i)
	{
		text[i].Char.AsciiChar = x[i];
		text[i].Attributes=(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	}
}

/**
 * \brief two�y string d�ugo�ci 1. czarne t�o bia�y text
 * \param x D�ugo�� stringa RAZEM z mienscem na Null
 */
CharInfoString::CharInfoString(const int x)
{
	cursor = 0;
	size = x;

	text = new CHAR_INFO[size];

	for (unsigned int i = 0; i < size; ++i)
	{
		text[i].Char.AsciiChar = ' ';
		text[i].Attributes = (FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
	}
}

/**
 * \brief Stworz nowy CharinfoString z tym cstingiem
 * \param x String do zamiany
 */
void CharInfoString::operator=(char* x)
{
	cursor = 0;
	int size = 0;
	while (x[size] != 0)
	{
		size++;
	}
	size += 1;
	this->size = size;

	delete[] text;
	text = new CHAR_INFO[size];

	for (int i = 0; i < size; ++i)
	{
		text[i].Char.AsciiChar = x[i];
		text[i].Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
	}
}

/**
 * \brief stwo� nowy string d�ugo�ci 1 z tym znakiem
 * \param x znak do wstawiena
 */
void CharInfoString::operator=(const char x)
{
	cursor = 0;
	delete[] text;
	text = new CHAR_INFO;
	size = 1;
	text->Char.AsciiChar = x;
	text->Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
}

/**
 * \brief Zainicjalizuj stringa i ustaw wsz�dzie spacje;
 * \param x D�ugio�� Stringa.
 */
void CharInfoString::operator=(const int x)
{
	cursor = 0;
	this->size = x;

	delete[] text;
	text = new CHAR_INFO[size];

	for (unsigned int i = 0; i < size; ++i)
	{
		text[i].Char.AsciiChar = ' ';
		text[i].Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
	}
}

/**
 * \brief Wstaw te znaki za ostatnim replace char[s|infos]
 * \param x cString do wstawienia
 */
void CharInfoString::operator<<(const char* x)
{
	int size = 0;
	while (x[size] != 0)
	{
		size++;
	}

	if (cursor + size < this->size)
	{
		for (int i = cursor; i<cursor + size + 1; ++i)
		{
			this->text[i].Char.AsciiChar = x[i - cursor];
		}
		cursor += size;
	}
}

/**
 * \brief Wstaw ten znak za ostatnim setChar[s|infos]
 * \param x Znak do wstawienia
 */
void CharInfoString::operator<<(const char x)
{
	if (cursor+1<this->size)
	{
		this->text[cursor].Char.AsciiChar = x;
		cursor++;
	}
}

void CharInfoString::operator<<(const CharInfoString x)
{

	if (cursor + x.getSize() < this->size)
	{
		for (int i = cursor; i<cursor + x.getSize() + 1; ++i)
		{
			this->text[i].Char.AsciiChar = x[i - cursor].Char.AsciiChar;
			this->text[i].Attributes = x[i - cursor].Attributes;
		}
		cursor += x.getSize();
	}
}


/**
 * \brief Zwraca ten konkretny CHAR_INFO
 * \param x od zera liczony index znaku
 * \return CHAR_INFO.
 */
CHAR_INFO CharInfoString::operator[](const int x) const
{
	return text[x];
}

/**
 * \brief Por�wnaj 2 charinfostringi
 * \param a string do por�wnania
 * \return True je�eli char i attribute si� r�wnaj�
 */
bool CharInfoString::operator==(CharInfoString a) const
{
	if (a.getSize() == size)
	{
		for (unsigned int i = 0; i < size; ++i)
		{
			if (a[i].Char.AsciiChar!=text[i].Char.AsciiChar)
			{
				return false;
			}
			if (a[i].Attributes!=text[i].Attributes)
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

/**
 * \brief Oddaj atrybut konkretnego znaku.
 * \param index Kt�ry attrybut potrzeba?
 * \return WORD attrybut
 */
WORD CharInfoString::getAttribute(const int index) const
{
	return text[index].Attributes;
}

/**
 * \brief Ustaw atrybuty wielu znak�w naraz
 * \param Attribute jakie atrybuty?
 * \param start Znak Pocz�tkowy
 * \param howMuch Ilo�c znak�w do zamiany
 */
void CharInfoString::setAttributes(const WORD Attribute, const int start, int howMuch) const
{
	if (howMuch == -1) howMuch = size;
	for (int i = 0; i < howMuch; ++i)
	{
		text[i + start].Attributes = Attribute;
	}
}

/**
 * \brief Zwraca ilo�� znak�w w stringu
 * \return Int ile znak�w.
 */
unsigned CharInfoString::getSize() const
{
	return size;
}

/**
 * \brief Podmie� znaki zachowuj�c attrybuty.
 * \param start Z k�d zaczynam?
 * \param text cString null-Terminated.
 * \return 0 je�eli sukces -1 je�eli nie.
 */
int CharInfoString::setChars(const int start,char* text)
{
	int size = 0;
	while (text[size] != 0)
	{
		size++;
	}

	if (start + size < this->size)
	{
		for (int i = start; i<start+size+1; ++i)
		{
			this->text[i].Char.AsciiChar = text[i - start];
		}
		cursor = start + size;
	}
	else { return -1; }
	return 0;
}

/**
 * \brief Podmie� zkan Zachowuj�c Attrybuty
 * \param start Jaki znak zamieni�?
 * \param text znak do zamiany.
 * \return 0 je�eli sukces -1 je�eli nie.
 */
int CharInfoString::setChars(const int start, const char text) const
{
	if (start>=size)
	{
		return -1;
	}
	this->text[start].Char.AsciiChar = text;
	return 0;
}

/**
 * \brief zamie� ilo�� znak�w na jeden.
 * \param start Od k�d zacz��?
 * \param text na Jaki znak zamieniam?
 * \param ile Ile mam zamieni�?
 * \param attributes Jakie Atrybuty Ustawi�?(Opcjonalny)
 * \return 0 je�eli sukces -1 je�eli nie.
 */
int CharInfoString::setChars(const int start, const char text, const int ile, const WORD attributes)
{
	if (start + ile < this->size)
	{
		for (int i = start; i<start + ile + 1; ++i)
		{
			this->text[i].Attributes = attributes;
			this->text[i].Char.AsciiChar = text;
		}
		cursor = start + ile;
	}
	else { return -1; }
	return 0;
}

/**
 * \brief Zamie� cz�� stringa na Ten Podany.
 * \param start Ok k�d zacz��?
 * \param text CharInfoString do podstawienia
 * \return 0 je�eli sukces -1 je�eli nie.
 */
int CharInfoString::setCharInfos(const int start, const CharInfoString text)
{
	if (start + text.size < this->size)
	{
		for (unsigned int i = 0; i < text.size; ++i)
		{
			this->text[i+start].Char.AsciiChar = text[i].Char.AsciiChar;
			this->text[i+start].Attributes = text[i].Attributes;
		}
		cursor = start + size;
	}
	else { return -1; }
	return 0;
}
